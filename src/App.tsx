import { useCallback, useEffect, useState } from "react";
import { HangmanDrawing } from "./HangmanDrawing";
import { HangmanWord } from "./HangmanWord";
import { Keyboard } from "./Keyboard";
import hangman from"../public/assets/Hangman.png";
import WordsArray from "./wordList.json";

import "./app.css";

function getWord() {
  return WordsArray[Math.floor(Math.random() * WordsArray.length)];
}
function App() {
  const [wordToGuess, setWordToGuess] = useState(getWord);
  const [guessedLetters, setGuessedLetters] = useState<string[]>([]);
 
  const incorrectLetters = guessedLetters.filter(
    (letter) => !wordToGuess.includes(letter)
  );

  const isLoser = incorrectLetters.length >= 6;
  const isWinner = wordToGuess
    .split("")
    .every((letter) => guessedLetters.includes(letter));

  const addGuessedLetter = useCallback(
    (letter: string) => {
      if (guessedLetters.includes(letter) || isLoser || isWinner) return;

      setGuessedLetters((currentLetters) => [...currentLetters, letter]);
    },
    [guessedLetters, isWinner, isLoser]
  );

  useEffect(() => {
    const handler = (e: KeyboardEvent) => {
      const key = e.key;
      if (!key.match(/^[a-ö]$/)) return;

      e.preventDefault();
      addGuessedLetter(key);
    };

    document.addEventListener("keypress", handler);

    return () => {
      document.removeEventListener("keypress", handler);
    };
  }, [guessedLetters]);

  useEffect(() => {
    const handler = (e: KeyboardEvent) => {
      const key = e.key;
      if (key !== "Enter") return;

      e.preventDefault();
      setGuessedLetters([]);
      setWordToGuess(getWord());
    };

    document.addEventListener("keypress", handler);

    return () => {
      document.removeEventListener("keypress", handler);
    };
  }, []);

  return (
    <div className="container">
      
      <div className="cont">
        <div className="box">
       
          <div
            style={{
              maxWidth: "600px",
              display: "flex",
              flexDirection: "column",
              gap: "2rem",
              margin: "0 auto",
              alignItems: "center",
            }}
          >
            
            <div style={{ fontSize: "1.5rem" }}>
              {isWinner && (
                <div>
                  <div className="ppW">You found the Word</div>
                  <button
                    className="btn-refrash"
                    onClick={() => window.location.reload()}
                  >
                    Play again
                  </button>
                </div>
              )}
              {isLoser && (
                <div>
                  <div className="ppL">You are loser</div>
                  <button
                    className="btn-refrash"
                    onClick={() => window.location.reload()}
                  >
                    Play again
                  </button>
                </div>
              )}
            </div>
            <div className="top">
            <img src={hangman} width="100px"/><div className="title">Find the name of the game!</div>
            </div>
            <HangmanDrawing numberOfGuesses={incorrectLetters.length} />
            <HangmanWord
              reveal={isLoser}
              guessedLetters={guessedLetters}
              wordToGuess={wordToGuess}
            />

            <div style={{ alignSelf: "stretch" }}>
              <Keyboard
                disabled={isWinner || isLoser}
                activeLetters={guessedLetters.filter((letter) =>
                  wordToGuess.includes(letter)
                )}
                inactiveLetters={incorrectLetters}
                addGuessedLetter={addGuessedLetter}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
